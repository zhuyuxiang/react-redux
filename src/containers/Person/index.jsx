import React, { Component } from 'react'
import {connect} from 'react-redux'
import {addPersonAction} from '../../redux/actions/person'
import {nanoid} from 'nanoid'
class Person extends Component {
  addPerson =() => {
    const name = this.input1.value
    const age = this.input2.value
    const data = {id:nanoid(),name,age}
    this.props.add(data)
    this.input1.value = ''
    this.input2.value = ''
  }
  render() {
    return (
      <div>
        <h2>我是person组件,上方求和为:{this.props.count}</h2>
        <input ref={e=>this.input1 = e} type="text"/>
        <input ref={e=>this.input2 = e} type="text"/>
        <button onClick={this.addPerson}>+</button>
        <ul>
          {
            this.props.person.map(item=>{
              return <li key={item.id}>{item.name}---{item.age}</li>
            })
          }
        </ul>
      </div>
    )
  }
}
export default connect(
  state=>({
    person:state.person,
    count:state.count
  }),
  {
    add:addPersonAction
  }
)(Person)


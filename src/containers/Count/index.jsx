import React, { Component } from 'react'
import {connect} from 'react-redux'
import {increment,decrement,incrementAsync} from '../../redux/actions/count'
class Count extends Component {
  increment = () => {
    const value = this.selectVlaue.value*1
    this.props.increment(value)
  }
  decrement = () => {
    const value = this.selectVlaue.value*1
    this.props.decrement(value)
  }
  oddIncrement = () => {
    const value = this.selectVlaue.value*1
    if(this.props.count % 2 !== 0){
      this.props.increment(value)
    }
  }
  asyncIncrement = () => {
    const value = this.selectVlaue.value*1
    this.props.incrementAsync(value,900)
  }
  render() {
    return (
      <div>
        <h2>我是Count组件,下方数据的长度为:{this.props.person}</h2>
        当前求和为:???{this.props.count}
        <select ref={e=>this.selectVlaue=e}>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
        </select>
        <button onClick={this.increment}>+</button>
        <button onClick={this.decrement}>-</button>
        <button onClick={this.oddIncrement}>奇数加</button>
        <button onClick={this.asyncIncrement}>异步加</button>
      </div>
    )
  }
}
export default connect(
  state=>({count:state.count,person:state.person.length}),
  {
    increment,
    decrement,
    incrementAsync
  }
)(Count)

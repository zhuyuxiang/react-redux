// 这个文件用于汇总所有的reducer
//引入为count组件服务的reducer,这里为了下面能简写,直接取名为count
import count from './count'
import person from './person'
import {combineReducers} from 'redux'
export default combineReducers({
  count,
  person
})
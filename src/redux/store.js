//引入createStore,专门用于创建redux中最核心的store对象
//applyMiddleware执行中间件的
//combineReducers用于合并多个reducer
import {createStore,applyMiddleware} from 'redux'
//引入汇总后的reducer
import reducer from './reducers'
// 引入redux-thunk,用于支持异步的action
import thunk from 'redux-thunk'
// 引入redux-devtools-extension开发者工具
import {composeWithDevTools} from 'redux-devtools-extension'

export default createStore(reducer,composeWithDevTools(applyMiddleware(thunk)))